module UI
  module Casino
    # Base UI for the Slot Machines
    class BaseUI < GenericBase
      private

      def background_filename
        'casino/base'
      end
    end
  end
end
